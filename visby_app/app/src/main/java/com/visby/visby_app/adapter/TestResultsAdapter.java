package com.visby.visby_app.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.visby.visby_app.R;
import com.visby.visby_app.activity.TestResultsListActivity;
import com.visby.visby_app.models.TestResultsModel;

import java.util.List;

public class TestResultsAdapter extends RecyclerView.Adapter<TestResultsAdapter.MyViewHolder> {
    private Context context;

    List<TestResultsModel> mTestResultList;

    public TestResultsAdapter(List<TestResultsModel> mTestResultList) {
        this.mTestResultList = mTestResultList;
        this.context = context;

    }

    @NonNull
    @Override
    public TestResultsAdapter.MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.test_results_item_list, parent, false);

        return new TestResultsAdapter.MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(@NonNull final TestResultsAdapter.MyViewHolder holder, final int position) {

        TestResultsModel testResultsList = mTestResultList.get(position);

        holder.mTvName.setText(testResultsList.getName());

      /*  Glide.with(context)
                .load(mCoachesList.get(position).getPhotoUrl())
                .centerCrop()
                .placeholder(R.drawable.profile_image_coach)
                .into(holder.IvCoach); */
//        holder.Btn_coach_select.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                // ((TestResultsListActivity)holder.itemView.getContext()).getActivityPlan(position);
//            }
//        });


    }


    @Override
    public int getItemCount() {
        if (mTestResultList != null) {
            if (mTestResultList.size() > 0) {
                return mTestResultList.size();
            } else {
                return 0;
            }
        } else {
            return 0;
        }

    }


    @Override
    public int getItemViewType(int position) {
        return super.getItemViewType(position);
    }

    class MyViewHolder extends RecyclerView.ViewHolder {

        TextView mTvName, mTvLocation, mTvId, mTvDob, mTvDateTime;
        Button Btn_coach_select;
        ImageView IvCoach;

        public MyViewHolder(@NonNull View itemView) {
            super(itemView);
            mTvName = (TextView) itemView.findViewById(R.id.tvName);
            mTvLocation = (TextView) itemView.findViewById(R.id.tvLocation);
            mTvId = (TextView) itemView.findViewById(R.id.tvId);
            mTvDob = (TextView) itemView.findViewById(R.id.tvDob);
            mTvDateTime = (TextView) itemView.findViewById(R.id.tvDateTime);
        }
    }


}