package com.visby.visby_app.activity;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.visby.visby_app.R;
import com.visby.visby_app.SymptomChecker.SymptomCheckerActivity;

public class TestResultStartActivity extends AppCompatActivity {

    //    private ImageView backBtn;
    private ImageView mHomeBtn;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_test_result_start);
        initViews();
        bindListener();
    }

    private void initViews() {
//        backBtn = findViewById(R.id.back_btn);
        ImageView BtnBack = findViewById(R.id.BtnBack);
        BtnBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });

        mHomeBtn = findViewById(R.id.home_btn);
        TextView mTvActionbarTitle = findViewById(R.id.TvActionbarTitle);
        TextView tvCaptureResult = findViewById(R.id.tvCaptureResult);
//        tvCaptureResult.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                Intent intent = new Intent(TestResultStartActivity.this, CaptureResultActivity.class);
//                startActivity(intent);
//            }
//        });

        mHomeBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(TestResultStartActivity.this, HomeActivity.class);
                startActivity(intent);
            }
        });
//        TextView mBtnTitle = findViewById(R.id.tvBtnTitle);

        mTvActionbarTitle.setText("PATIENT: JONE COOPER");
//        mBtnTitle.setText(getResources().getString(R.string.signin_btn_name));
        soup.neumorphism.NeumorphCardView mBtnCustom = findViewById(R.id.BtnCustom);
        mBtnCustom.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(TestResultStartActivity.this, CaptureResultActivity.class);
                startActivity(intent);
            }
        });
    }

    private void bindListener() {
//        backBtn.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View view) {
//                finish();
//            }
//        });
    }
}