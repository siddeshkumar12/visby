package com.visby.visby_app.SymptomChecker;

import androidx.appcompat.app.AppCompatActivity;
import androidx.cardview.widget.CardView;
import androidx.fragment.app.FragmentTransaction;

import android.content.Intent;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.view.View;
import android.view.WindowManager;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.TextView;

import com.visby.visby_app.QuickReferenceGuid.QuickReferenceGuidActivity;
import com.visby.visby_app.R;
import com.visby.visby_app.activity.HomeActivity;
import com.visby.visby_app.activity.MainActivity;
import com.visby.visby_app.fragments.Fragment1;
import com.visby.visby_app.fragments.Fragment2;
import com.visby.visby_app.fragments.Fragment3;

public class SymptomCheckerActivity extends AppCompatActivity {

    private FrameLayout container;
    private TextView mContinueButton;
    private Integer count = 1;
    private ImageView backBtn;
    private soup.neumorphism.NeumorphCardView mContinue;
    private ImageView mHomeBtn;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_symptom_checker);

        this.getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN);
        TextView mTvActionbarTitle = findViewById(R.id.TvActionbarTitle);
        mTvActionbarTitle.setText("SYMPTOM CHECKER \n Patient: Jane Cooper");
        mContinue = findViewById(R.id.BtnCustom);
        container = findViewById(R.id.container);
        backBtn = findViewById(R.id.back_btn);
        mContinueButton = findViewById(R.id.tvBtnTitle);
        mHomeBtn = findViewById(R.id.home_btn);
//        Drawable img = getResources().getDrawable(R.drawable.ic_baseline_arrow_right);
//        mContinueButton.setCompoundDrawablesWithIntrinsicBounds(null,null,img,null);

        // Begin the transaction
        FragmentTransaction ft = getSupportFragmentManager().beginTransaction();
        // Replace the contents of the container with the new fragment
        ft.replace(R.id.container, new SymptomCheckerFragment1());
        // or ft.add(R.id.your_placeholder, new ABCFragment());
        // Complete the changes added above
        ft.commit();

        mHomeBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(SymptomCheckerActivity.this, HomeActivity.class);
                startActivity(intent);
            }
        });

        backBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(count == 3){
                    count = count-1;
                    mContinueButton.setText("Continue");
                    moveToSecond();
                }else if(count == 2){
                    count = count-1;
                    moveTofirst();
                }else if(count == 1){
                    count = count-1;
                    finish();
                }
            }
        });
        mContinue.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                if(count == 1){
                    count = count+1;
                    moveToSecond();
                }else if(count == 2){
                    count = count+1;
                    mContinueButton.setText("Visby Test");
                    moveToThird();
                }else if(count == 3){
                    Intent intent = new Intent(SymptomCheckerActivity.this, QuickReferenceGuidActivity.class);
                    startActivity(intent);
                }
            }
        });
//        mContinueButton.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View view) {
//
//                count = count+1;
//
//                if(count == 1){
//                    moveToSecond();
//                }else if(count == 2){
//                    moveToThird();
//                }else{
//
//                }
////                FragmentTransaction ft = getSupportFragmentManager().beginTransaction();
////                ft.replace(R.id.container, new Fragment2(), "Fragment2");
////                ft.commit();
//
//            }
//        });
    }

    @Override
    protected void onResume() {
        super.onResume();

        mContinueButton.setText("Continue");
        if(count == 3){
            mContinueButton.setText("Visby Test");
        }
    }

    private void moveTofirst(){
        FragmentTransaction ft = getSupportFragmentManager().beginTransaction();
        ft.replace(R.id.container, new SymptomCheckerFragment1(), "SymptomCheckerFragment1");
        ft.commit();
    }

    private void moveToSecond(){
        FragmentTransaction ft = getSupportFragmentManager().beginTransaction();
        ft.replace(R.id.container, new SymptomCheckerFragment2(), "SymptomCheckerFragment2");
        ft.commit();
    }

    private void moveToThird(){
        FragmentTransaction ft = getSupportFragmentManager().beginTransaction();
        ft.replace(R.id.container, new SymptomCheckerFragment3(), "SymptomCheckerFragment3");
        ft.commit();
    }
}