package com.visby.visby_app.activity;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.DefaultItemAnimator;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;

import com.visby.visby_app.R;
import com.visby.visby_app.adapter.TestResultsAdapter;
import com.visby.visby_app.models.TestResultsModel;

import java.util.ArrayList;
import java.util.List;

public class TestResultsListActivity extends AppCompatActivity {

    private RecyclerView mRecyclerViewTestResult;
    List<TestResultsModel> mTestResultArrayLists = new ArrayList<>();
    private TestResultsAdapter mTestResultsAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_test_results_list);
        initViews();
    }

    private void initViews(){
        mRecyclerViewTestResult = findViewById(R.id.recyclerViewTestResult);
       ImageView BtnBack = findViewById(R.id.BtnBack);
        BtnBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });

        TestResultsModel testResults = new TestResultsModel();
        testResults.setName("LOKESH G");
        testResults.setId("2323232");
        testResults.setLocation("Banglore, KA");
        testResults.setDob("12/01/2002");
        testResults.setDate_time("12/01/2021, 10:20 AM");
        mTestResultArrayLists.add(testResults);

        testResults.setName("SIDDESH K");
        testResults.setId("2323232");
        testResults.setLocation("Banglore, KA");
        testResults.setDob("12/01/2002");
        testResults.setDate_time("12/01/2021, 10:20 AM");
        mTestResultArrayLists.add(testResults);

        testResults.setName("Jane Cooper");
        testResults.setId("2323232");
        testResults.setLocation("San Jose, CA");
        testResults.setDob("12/01/2002");
        testResults.setDate_time("12/01/2021, 10:20 AM");
        mTestResultArrayLists.add(testResults);

        mTestResultsAdapter = new TestResultsAdapter(mTestResultArrayLists);
        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(getApplicationContext().getApplicationContext());
        mRecyclerViewTestResult.setLayoutManager(mLayoutManager);
        mRecyclerViewTestResult.setItemAnimator(new DefaultItemAnimator());
        mRecyclerViewTestResult.setAdapter(mTestResultsAdapter);


    }


}