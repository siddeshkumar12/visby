package com.visby.visby_app.activity;

import androidx.appcompat.app.AppCompatActivity;
import androidx.cardview.widget.CardView;
import androidx.core.content.ContextCompat;

import android.content.Intent;
import android.graphics.Color;
import android.os.Build;
import android.os.Bundle;
import android.view.View;
import android.view.Window;
import android.widget.TextView;

import com.visby.visby_app.R;

public class SignInActivity extends AppCompatActivity {

    private CardView BtnClick;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (Build.VERSION.SDK_INT < Build.VERSION_CODES.M) {

        }
        setContentView(R.layout.activity_sign_in);

        initViews();
    }

    private void initViews() {
//        TextView mBtnTitle = findViewById(R.id.tvBtnTitle);
        soup.neumorphism.NeumorphCardView BtnSignIn = findViewById(R.id.BtnSignIn);


        BtnSignIn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(SignInActivity.this, TestingLocationActivity.class);
                startActivity(intent);
            }
        });


    }

}