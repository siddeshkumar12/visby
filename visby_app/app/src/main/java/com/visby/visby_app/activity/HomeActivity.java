package com.visby.visby_app.activity;

import androidx.appcompat.app.AppCompatActivity;
import androidx.cardview.widget.CardView;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.TextView;

import com.visby.visby_app.R;

public class HomeActivity extends AppCompatActivity {

    private CardView mBtnCustom;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_home);
        initViews();
    }

    private void initViews() {
        TextView mTvActionbarTitle = findViewById(R.id.TvActionbarTitle);
        TextView tvBtnStartVisbyTest = findViewById(R.id.tvBtnStartVisbyTest);
        TextView tvTestResults = findViewById(R.id.tvTestResults);

        tvBtnStartVisbyTest.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(HomeActivity.this, MainActivity.class);
                startActivity(intent);
            }
        });

        tvTestResults.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(HomeActivity.this, TestResultsListActivity.class);
                startActivity(intent);
            }
        });
//        TextView mBtnTitle = findViewById(R.id.tvBtnTitle);
//        mBtnCustom = findViewById(R.id.BtnCustom);
        mTvActionbarTitle.setText("Hello ANNA");
//        mBtnTitle.setText(getResources().getString(R.string.signin_btn_name));

//        mBtnCustom.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                Intent intent = new Intent(HomeActivity.this, HomeActivity.class);
//                startActivity(intent);
//            }
//        });
    }


}