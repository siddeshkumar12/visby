package com.visby.visby_app.activity;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.drawable.Drawable;
import android.media.Image;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.squareup.picasso.MemoryPolicy;
import com.squareup.picasso.NetworkPolicy;
import com.squareup.picasso.Picasso;
import com.visby.visby_app.R;

public class TestResultActivity extends AppCompatActivity {

    private ImageView mHomeBtn;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_test_result);
        initViews();
        bindListener();
    }

    private void initViews() {

        soup.neumorphism.NeumorphCardView BtnPtint = findViewById(R.id.BtnPtint);
        soup.neumorphism.NeumorphCardView BtnShare = findViewById(R.id.BtnShare);

        TextView mTvActionbarTitle = findViewById(R.id.TvActionbarTitle);
        mTvActionbarTitle.setText("TEST RESULT");

        mHomeBtn = findViewById(R.id.home_btn);
        ImageView BtnBack = findViewById(R.id.BtnBack);
        BtnBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
        ImageView IvtestResult = findViewById(R.id.IvtestResult);
        Intent intent = getIntent();

        String path = null;

        path = getIntent().getStringExtra("image");
        if (path != null) {
            Drawable image = Drawable.createFromPath(path);
            IvtestResult.setImageDrawable(image);
        }

    }

    private void bindListener(){
        mHomeBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(TestResultActivity.this, HomeActivity.class);
                startActivity(intent);
            }
        });
    }
}