package com.visby.visby_app.activity;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;

import com.visby.visby_app.R;

public class LandingActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_landing);
    }
}