package com.visby.visby_app.SymptomChecker;

import android.os.Bundle;

import androidx.fragment.app.Fragment;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.CompoundButton;

import com.visby.visby_app.R;

/**
 * A simple {@link Fragment} subclass.
 * Use the {@link SymptomCheckerFragment1#newInstance} factory method to
 * create an instance of this fragment.
 */
public class SymptomCheckerFragment1 extends Fragment {

    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";

    private CheckBox feverCheckBoxYes, feverCheckBoxNo, coughCheckBoxYes, coughCheckBoxNo;

    // TODO: Rename and change types of parameters
    private String mParam1;
    private String mParam2;

    public SymptomCheckerFragment1() {
        // Required empty public constructor
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @param param1 Parameter 1.
     * @param param2 Parameter 2.
     * @return A new instance of fragment SymptomCheckerFragment1.
     */
    // TODO: Rename and change types and number of parameters
    public static SymptomCheckerFragment1 newInstance(String param1, String param2) {
        SymptomCheckerFragment1 fragment = new SymptomCheckerFragment1();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_symptom_checker1, container, false);
        feverCheckBoxYes = view.findViewById(R.id.fever_yes);
        feverCheckBoxNo = view.findViewById(R.id.fever_no);

        coughCheckBoxYes = view.findViewById(R.id.cough_yes);
        coughCheckBoxNo = view.findViewById(R.id.cough_no);

        return view;
    }

    @Override
    public void onResume() {
        super.onResume();
        feverCheckBoxYes.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
                feverCheckBoxNo.setChecked(false);
            }
        });

        feverCheckBoxNo.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
                feverCheckBoxYes.setChecked(false);
            }
        });

        coughCheckBoxYes.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
                coughCheckBoxNo.setChecked(false);
            }
        });

        coughCheckBoxNo.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
                coughCheckBoxYes.setChecked(false);
            }
        });
    }
}